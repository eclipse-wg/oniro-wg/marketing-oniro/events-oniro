Oniro: Rejoindre la communauté Oniro ...
... pour participer au système d'exploitation OpenSource pour TOUT les objets connectés

Premièrement, le projet Eclipse Oniro y sera présenté
on y apprendra sa différentiation et
son positionnement parmi d'autres projets open source
tel que Linux, OpenEmbedded/Yocto ou Zephyr
mais aussi OpenHarmony d'OpenAtom.

Ensuite quelques cas d'usages seront démontrés,
et nous verrons en quoi l'approche des Blueprints Oniro
est une base solide pour aller au delà de la preuve de concept
et devenir une base pour créer des produits IoT.

Finalement, différentes approches pour participer a Oniro vous seront proposées.
Du fait de son ouverture il est déja possible de rejoindre le working group pour décider du futur d'Oniro.
Il est également possible d'évaluer la plateforme et même d'y contribuer
ou bien de créer une simple application.
Également quelques recettes pour l'élaboration de prototypes seront partagées
pour inspirer les "markers" parmi vous.
