## FOSDEM report from toscalix

I attended to FOSDEM 2022 which was online this year, for the second time in a 
row. As you probably know by now Oniro delivered quiet a few talks at FOSDEM 
2022. 

In relation with the content, Oniro was well managed by Philippe Coval. We 
used the marketing gitlab repo[1], tickets and the FOSDEM wiki page[2] as 
coordination tools. From my point of view, things went well there. His 
experience on this event and the fact that many of the people involved did 
something similar last year, helped. 

In addition to the large amount of talks, we managed to have the stand ready 
ahead of time which took away some last minute drama that is so popular when 
preparing these events.  

Thank you Philippe for your effort and passion.

I spent most of the Saturday morning at the Eclipse stand and lurking around 
stands, including Oniro. The most crowded ones that I saw were Debian and KDE 
with between 150 and 200 people during the morning. KDE had demo sessions 
scheduled during the weekend which was very successful. A good idea we can 
copy in the future.

I counted a couple of times at Oniro stand during the Saturday morning between 
50 and 60 people, which is a good number for being a new project. FOSDEM space 
at matrix had a little over 2k accounts on Saturday morning. Can somebody 
confirm?

I joined FOSDEM again on Sunday at lunch until the end. I shared some time 
between, again, Eclipse and Oniro stands, my talk and some other talks from 
people I know. Except for a couple of cases, I decided to watch Oniro talks 
during this week instead of at the event so I could talk to people, following 
the same trend I do on f2f editions.

My talk, like most lightning talks, was poorly attended. I do not have a 
number but I know most of the people that were there. It was a good exercise 
for me though to condense the most relevant aspects of Oniro in 15 minutes. 
Given the amount of technical talks Oniro team delivered, I left the technical 
aspects out of mine. I will need a couple more iterations to consolidate it 
but as a target, 15 min talk is ideal.

A takeaway for me was the high number of talks I saw that were edited/recorded 
using OBS/kdenlive. I used zoom so I feel I am a little behind on this front 
so I will invest some time in catching up.

Given the number of rooms and stands, following the event through matrix 
(especially with a client) was chaotic. I wonder if this size is really 
manageable online. I think though that matrix for Oniro will work extremely 
well. I installed Element on my laptop through flatpak and I am amazed how 
much resources it eats, by the way.  

In order to understand what a modern linux based OS system is, I frequently 
put two distros as example: openSUSE Tumbleweed and freedesktop-sdk. There are 
others but these two I know well. Javier Jardon, one of the lead persons at 
freedesktop-sdk was present at my talk and we agreed on having a shared 
session to learn about each other. It wouldn't be the first time I do this. 
Other times it has been welcomed by engineers.

Another point I put some effort on was to analyse how mastodon reacts to 
FOSDEM.

As some of you know, mastodon is gaining traction among hackers and 
developers. Some people I know have reported me that they have 1 to 10 
followers in mastodon compared to Twitter.  The profile of the audience is 
very interesting for open source projects, in my opinion.  mastodon.social, 
for instance, have almost 700k accounts already.

I was surprised to see the traffic FOSDEM generated at mastodon. Philippe is 
way more active in this social media than I am so I will let him confirm or 
deny my impression.

I am curious on how Eclipse Foundation did on Linkedin in relation to FOSDEM. 
I will ask. I published a couple of entries and got less relative attention 
than those I did in Twitter. Does this match with your perception? I retweeted 
and shared some of the media messages or marketing team and the EF team put 
together on Twitter. Looking forward to evaluate how that aspect went.

In general, the technical elements of the conference worked well, given its 
size. The Q&A part of the talks were well managed and easy to follow, at least 
for me.

Again, thank you all for putting effort at FOSDEM. I hope next year we go back 
to the cold, wet, crowded and uncomfortable f2f FOSDEM. And if not, this 
year's experience will sure be valuable. 

[1] <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/proposal-incubation-stage-oniro/proposal-incubation-oniro-marketing>
[2] <https://gitlab.eclipse.org/groups/eclipse-wg/oniro-wg/proposal-incubation-stage-oniro/-/wikis/Events/FOSDEM>
