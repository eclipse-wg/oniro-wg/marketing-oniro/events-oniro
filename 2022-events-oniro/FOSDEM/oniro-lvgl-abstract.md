# Oniro LVGL

## Track

Graphics

https://fosdem.org/2022/schedule/tracks/


## Title

LVGL: A versatile UI toolkit for MCU & CPU
  

## Subtitle

An UI framework for Eclipse Onino a cross kernel OS


## Abstract

LVGL is an open-source graphics library to create embedded GUI.

By its flexibility, this toolkit is well matching Oniro project's requirements
to build a multiple kernel OS for IoT devices
or enable interactions in a multiple devices environment.


## Description

UI toolkits are well established in CPU world (desktop, mobile),
but what about in more constrained system ?

Often tied to one vendor, the choice is usually limited.
However OpenSource project LVGL provides everything developers
need to create embedded GUI with easy-to-use graphical elements,
beautiful visual effects and low memory footprint.

This library is compact and flexible enough
to be thread through a regular Linux system.
Of course, this will not compete with other popular frameworks like Qt or GTK,
but it has some advantages
worth sharing: 
where portability matters more than eye candy features.
  
Oniro's Bitbake recipes enable makers to build products with user interface
in a Yocto environment that can target different kernels or graphics systems.

Oniro's blueprints will illustrate integration details to match a product requirement.

Demos of vending machine (running on Linux+Wayland)
or keypad (on Zephyr MCU OS) will be shown too.


## Persons

Philippe Coval + guest Gábor Kiss-Vámosi Founder of LVGL

For years, Philippe Coval is acting as professional OpenSource Engineer,
but he has been involved into software communities since his teenage "Amiga" years.

Over decades, he has contributed to many projects from operating systems
(Debian, MeeGo, Tizen) to IoT frameworks (IoTivity, WebThings) and more.

Creative mind he also shared many proof of concepts in various domains
at various occasions (FOSDEM, ELC, MozFest...),

Currently contributing to the Oniro project,
he is always happy to help and open for new cooperation.

Feel free to reach him at: https://purl.org/rzr/

Gábor Kiss-Vámosi is the founder and one of the main contributors
of an open-source graphics library, called LVGL.
He is also the CEO of LVGL Kft which is doing UI implementation,
graphics design and consulting services related to LVGL.


## Event type

20/30 min talk + 5 min QA


## Links

LVGL : Light and Versatile Graphics Library
https://lvgl.io

LVGL Sources:
https://github.com/lvgl/lvgl

Oniro: The Distributed Operating System That Connects Consumer Devices Big and Small
https://oniroproject.org/

Oniro sources
https://booting.oniroproject.org/

Oniro Videos
https://www.youtube.com/watch?v=p-gSvehb-As&list=PLy7t4z5SYNaQBDReZmeHAknEchYmu0LLa#OniroPlaylist
